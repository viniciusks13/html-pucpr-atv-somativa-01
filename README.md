# html-pucpr-atv-somativa-01
Projeto destinado para a atividade somativa 01 do curso *Superior de Tecnologia em Análise e Desenvolvimento de Sistemas*.

## Sobre
O projeto consiste em uma aplicação bem simples escrita em HTML e CSS, utilizando o Materialize CSS para colocar alguns compenentes na tela e atender aos requisitos exigidos dentro da atividade.
Tecnologias utilizadas:
- HTML;
- CSS (Materialize CSS);
